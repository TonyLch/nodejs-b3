const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const formatMessage = require('./utils/messages');
const { userToList, removeFromList } = require('./utils/users');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(express.static(path.join(__dirname, 'public')));

const botName = 'OnlyChat Bot';

// Run when client connects
io.on('connection', socket => {

    socket.on('joinChat', username => {
        var users = userToList(username);

        io.emit('users', users);

        // Broadcast to the user when connects
        socket.emit('message', formatMessage(botName, 'Welcome to OnlyChat!'));
    
        // Broadcast (to everybody except the user) when a user connects
        socket.broadcast.emit('message', formatMessage(botName, `${username} has joined the chat!`));
    
        socket.on('chatMessage', msg => {
            // Broadcast to everybody
            io.emit('message', formatMessage(username, msg));
        });

        // Run when client disconnects
        socket.on('disconnect', () => {
            io.emit('message', formatMessage(botName, `${username} has left the chat...`));
            removeFromList(username);
            users = userToList(username);
            io.emit('users', users);
        });
    });

});

const PORT = 3000 || process.env.PORT;

server.listen(PORT, () => console.log(`Server is running on PORT: ${PORT} \nApp running at:\n- Local: http://localhost:${PORT}/`));