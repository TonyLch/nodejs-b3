const chatroomForm = document.getElementById('chatroom-form');
const chatMessages = document.querySelector('.chatroom-messages');

// Get username
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const username = urlParams.get('username');

const socket = io();

socket.emit('joinChat', username);

socket.on('users', users => {
    clearList();
    users.forEach(element => {
        usernameToList(element);
    });
});

socket.on('message', message => {
    console.log(message);
    outputMessage(message);

    // Scroll pin down
    chatMessages.scrollTop = chatMessages.scrollHeight;
});

// Message submit
chatroomForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // Get text message from input
    const msg = e.target.elements.msg.value;

    // Emit message to server
    socket.emit('chatMessage', msg);

    // Clear input & refocus on input
    e.target.elements.msg.value = '';
    e.target.elements.msg.focus();
});

// Output message to DOM
function outputMessage(message) {
    const li = document.createElement('li');
    li.classList.add('message');
    li.innerHTML = `<h3>${message.username}</h3>
                    <p>${message.text}</p>`;
    document.getElementById('messages').appendChild(li);
}

// Clear user list
function clearList() {
    const ul = document.getElementById("users");
    ul.innerHTML = '';
}

// Username to list in DOM
function usernameToList(username) {
    const li = document.createElement('li');
    li.innerHTML = `${username}`;
    document.getElementById('users').appendChild(li);
}