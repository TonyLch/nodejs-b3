# Get Started with Express

Exercise :

- Réaliser un chat en temps réel (sans persistances de données)
- Le chat fonctionne en localhost et utiliser deux onglets de navigation pour simuler deux utilisateur du chat
- Lancer plein de requete pour vérifier que le code ne soit pas bloquant (synchrone)

- [x] Chat en temps réel
- [x] Simulation de 2 utilisateurs sur le tchat via 2 onglets
- [ ] Code non bloquant vérifié (synchrone)

## Instructions

Run this command line in this folder : 

    npm i
    node .\server.js

Then CTRL + LEFT CLICK on the link.

Now that you're on the app, duplicate the page. Enter a username and press ENTER. You should enter in the chatroom. Then, go on the other page, enter a username and send a message. The message should display on both user's chat.

## Features

- [x] Several simultaneous users
- [x] Display message for every user in the chatroom
- [x] Display username of each message
- [ ] Display user list