// Exercise 1

// First step
// Initialize two numbers as string, add them together while they still are type of string

let a = "2";
let b = "5";

console.log("First step : \n");
console.log("Type of : " + typeof((parseInt(a, 10) + parseInt(b, 10)).toString()));
console.log("Result : " + (parseInt(a, 10) + parseInt(b, 10)).toString());
console.log("\n----------\n");


// Second step
/* 
    Create a function add(string)
    string should be a string of number separated by comas
    If string is empty, return 0
    In the other case, return the sum of those numbers
*/

function add(number) {

    if(number === '')
        return '0';
    else if (number.length > 0)
        return number.split(',').reduce((a, b)=> parseFloat(a, 10) + parseFloat(b, 10)).toFixed(1).toString();

    return number;
}

const result = add('1.1,2.2');

console.log("Second step : \n");
console.log("Type of : " + typeof(result));
console.log("Result : " + result);
console.log("\n----------\n");


// 